(function ($) {
  Drupal.behaviors.textarea_limit = {
    attach: function (context, settings) {
      // Loop textarea-limit fields and skip processed.
      $('.textarea-limit:not(.textarea-limit-processed)').each(function () {
        // Get the limit.
        var limit = parseInt($(this).attr('data-textarea-limit'));
        if (limit > 0) {
          // Get the textarea limit id.
          var textarea_limit_id = $(this).attr('id');
          if (textarea_limit_id) {
            // Attach limit js to textarea field.
            $(this).limit(limit, '#' + textarea_limit_id + '-counter');
            $(this).addClass('textarea-limit-processed');
          }
        }
      });
    }
  }
})(jQuery);
