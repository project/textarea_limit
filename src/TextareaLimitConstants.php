<?php

namespace Drupal\textarea_limit;

/**
 * Main class to hold constants.
 */
class TextareaLimitConstants {

  /**
   * Character limit constant.
   */
  const TL_CHAR_LIMIT_ID = 'textarea_limit_char_limit';

  /**
   * Use global limit constant.
   */
  const TL_USE_GLOBAL_LIMIT_ID = 'textarea_limit_use_global_limit';
}
