<?php

namespace Drupal\textarea_limit;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Trusted render callbacks.
 */
class TextareaLimitCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['limitPreRender'];
  }

  /**
   * Pre render function to set textarea_limit attributes.
   *
   * @param array|mixed $element
   *   The render array.
   *
   * @return array
   *   The processed render array.
   */
  public static function limitPreRender($element) {
    if (isset($element['#textarea_limit']) && $element['#textarea_limit'] === TRUE) {
      if (isset($element['#attributes']['data-textarea-limit']) && $element['#attributes']['data-textarea-limit'] > 0) {
        // Get char limit.
        $limit = $element['#attributes']['data-textarea-limit'];
        // Tack on remaining characters bit.
        $build = [
          '#limit' => $limit,
          '#id' => $element['#id'] . '-counter',
          '#theme' => 'textarea_limit_remaining',
        ];
        $element['#suffix'] = \Drupal::service('renderer')->renderPlain($build);

        // Tack on library/js.
        if (!in_array('textarea-limit', $element['#attributes']['class'])) {
          $element['#attributes']['class'][] = 'textarea-limit';
        }
        $element['#attached']['library'][] = 'textarea_limit/textarea_limit';
      }
    }
    return $element;
  }

}
